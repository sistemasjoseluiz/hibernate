/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
  HasCode, equals e interface Serializable e sua relação com o Hibernate
    - As melhores práticas do Hibernate recomendam ao máximo que os métodos
      equals e hasCode sejam sobrescritos nas classes persistíveis de seu 
      projeto.
      Estes métodos existem na Classe Objetc, contudo nesta Classe  só considera
      endereço de memória do objeto. Isso significa que se duas instâncias de um
      mesmo objeto, com os mesm dados internamente, forem comparados com == ou 
      obj1.equals(obj2), o resultado será false.
      
      Note: sempre que sobrescrever o equals o hasCode também deve ser sobrescrito. 
      Fonte.(Luckow e  Melo, 2015). 
      
*/

package br.com.auladehibernate.cliente;

import java.io.Serializable;
import java.util.GregorianCalendar;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="cliente")
@SequenceGenerator(name="seqCliente",sequenceName="seq_cliente")
public class Cliente implements Serializable{
    private static final long serialVersionUID = 5947125575414775991L;    
    
    @Id
    @GeneratedValue(generator="seqCliente",strategy= GenerationType.AUTO)
    @Column(name="id_cliente")
    private Integer idCliente;
    
    @Column(name="nome")
    private String nome;
    
    //@Column(name="data_nasc")
    //private GregorianCalendar  dataNascimento;

    public Integer getIdCliente() {
        return idCliente;
    }

    public String getNome() {
        return nome;
    }

    //public GregorianCalendar getDataNascimento() {
     //   return dataNascimento;
    //}

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    //public void setDataNascimento(GregorianCalendar dataNascimento) {
    //    this.dataNascimento = dataNascimento;
   // }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.idCliente);
        hash = 53 * hash + Objects.hashCode(this.nome);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.idCliente, other.idCliente)) {
            return false;
        }
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        return true;
    }

    
    
    
    
}
