/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.auladehibernate.cliente;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author joseluiz
 */
public class ClienteRN {
    private ClienteDAO clienteDAO = new ClienteDAO();
    
    public void salvar(Cliente cliente){        
        if (cliente.getIdCliente() == 0)
            clienteDAO.incluir(cliente);
       else
            clienteDAO.atualizar(cliente);       
    }
    
    public void deletar(Cliente cliente){
        clienteDAO.deletar(cliente);
    }
    
    public Cliente buscaCliente(Integer idCliente){
        return clienteDAO.buscaCliente(idCliente);
    }
    public List<Cliente> listaCliente(){
        return  clienteDAO.listaCliente();
    }
    public List<Cliente> listaClientePorNome(String texto){
        return clienteDAO.listaClientePorNome(texto);
    }
    
}
