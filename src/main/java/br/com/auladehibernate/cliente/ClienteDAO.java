/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.auladehibernate.cliente;

import br.com.auladehibernate.util.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author joseluiz
 */
public class ClienteDAO{    
    private Session sessao;
    private Transaction transacao;
    
    public void incluir(Cliente cliente) {
        
        try{            
            this.sessao = HibernateUtil.getSessionFactory().openSession();
            this.transacao = this.sessao.beginTransaction();
            this.sessao.save(cliente);           
            this.transacao.commit();
        }catch(HibernateException e){
            System.out.println("Não foi possível inserir o cliente. Erro: " + e.getMessage());
        }finally{
          this.sessao.close();                    
        }               
        
    }

    
    public void atualizar(Cliente cliente) {
        try{
            this.sessao = HibernateUtil.getSessionFactory().openSession();
            this.transacao = this.sessao.beginTransaction();
            this.sessao.update(cliente);           
             this.transacao.commit();
        }catch(HibernateException e){
            System.out.println("Não foi possível inserir o cliente. Erro: " + e.getMessage());
        }finally{
              try{
                    if(this.sessao.isOpen()){
                      this.sessao.close();
                    }
                }catch(Throwable e){
                    System.out.println("Erro ao fechar operação de inserção. Mensagem: " + e.getMessage());
                }                  
        }
    }
    
    
    public void deletar(Cliente cliente) {
        try{
            this.sessao = HibernateUtil.getSessionFactory().openSession();
            this.transacao = this.sessao.beginTransaction();
            this.sessao.delete(cliente);
            this.transacao.commit();
        }catch(HibernateException e){
            System.out.println("Não foi possível deletar o cliente. Erro: " + e.getMessage());
        }finally{
              try{
                    if(this.sessao.isOpen()){
                      this.sessao.close();
                    }
                }catch(Throwable e){
                    System.out.println("Erro ao fechar operação de deleção. Mensagem: " + e.getMessage());
                }                  
        }
    }

    
    public Cliente buscaCliente(Integer idCliente) {
        Cliente cliente = null;
        try{
            this.sessao = HibernateUtil.getSessionFactory().openSession();
            this.transacao = this.sessao.beginTransaction();
            Criteria filtro = this.sessao.createCriteria(Cliente.class);
            filtro.add(Restrictions.eq("idCliente",idCliente));
            cliente = (Cliente) filtro.uniqueResult();
            this.transacao.commit();
        }catch(HibernateException e){
            System.out.println("Não foi possível buscar o cliente. Erro: " + e.getMessage());
        }finally{
           this.sessao.close();
                       
          
         
        }
        return cliente;
    }

    
    public List<Cliente> listaClientePorNome(String texto) {
        List<Cliente> clientes = null;
        try{
            this.sessao = HibernateUtil.getSessionFactory().openSession();
            this.transacao = this.sessao.beginTransaction();
            Criteria filtro = this.sessao.createCriteria(Cliente.class);            
            filtro.add(Restrictions.ilike("nome",texto,MatchMode.START));
            //ANYWHERE: Qualquer parte na String.
            //END: Final da String.
            //EXACT: Combinação exata.
            //START: Inicio da String.                            
            clientes = filtro.list();
            this.transacao.commit();
        }catch(HibernateException e){
            System.out.println("Não foi possível listar os cliente. Erro: " + e.getMessage());
        }finally{
              try{
                    if(this.sessao.isOpen()){
                      this.sessao.close();
                    }
                }catch(Throwable e){
                    System.out.println("Erro ao fechar operação de listagem. Mensagem: " + e.getMessage());
                }                  
        }
        return clientes;
    }

   
    public List<Cliente> listaCliente() {      
        List<Cliente> clientes = null;
        try{
            this.sessao = HibernateUtil.getSessionFactory().openSession();            
            this.transacao = this.sessao.beginTransaction();
            Criteria filtro = this.sessao.createCriteria(Cliente.class);                        
            clientes = filtro.list();
            this.transacao.commit();
        }catch(HibernateException e){
            System.out.println("Não foi possível listar os cliente. Erro: " + e.getMessage());
        }finally{
              try{
                    if(this.sessao.isOpen()){
                      this.sessao.close();
                    }
                }catch(Throwable e){
                    System.out.println("Erro ao fechar operação de listagem. Mensagem: " + e.getMessage());
                }                  
        }
        return clientes;
                
    }    
    
}
