/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.auladehibernate.util;

import br.com.auladehibernate.cliente.Cliente;
import java.io.File;
import java.io.IOException;
import java.util.List;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author joseluiz
 */
public class Relatorio {
    
    public void geraRelatorio(List<Cliente> clientes){
        try{
            // Trabalhando com o iReport
            System.out.println("Gerando relatório…");         
            JasperReport pathjrxml = JasperCompileManager.compileReport("src/main/resources/relatorios/report1.jrxml");
            JasperPrint printReport = JasperFillManager.fillReport(pathjrxml, null, new JRBeanCollectionDataSource(clientes));
            JasperExportManager.exportReportToPdfFile(printReport, "src/main/resources/relatorios/report1.pdf");
            System.out.println("Relatorio gerado");
            java.awt.Desktop.getDesktop().open(new File("src/main/resources/relatorios/report1.pdf"));
            }catch(JRException e){
                System.out.println("Erro ao gerar Relatório " + e.getMessage());
            }catch(IOException o){
                System.out.println("Erro ao gerar abrir pdf " + o.getMessage());
        }    
    }
    
}
