/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*
  HasCode, equals e interface Serializable e sua relação com o Hibernate
    - As melhores práticas do Hibernate recomendam ao máximo que os métodos
      equals e hasCode sejam sobrescritos nas classes persistíveis de seu 
      projeto.
      Estes métodos existem na Classe Objetc, contudo nesta Classe  só considera
      endereço de memória do objeto. Isso significa que se duas instâncias de um
      mesmo objeto, com os mesm dados internamente, forem comparados com == ou 
      obj1.equals(obj2), o resultado será false.
      
      Note: sempre que sobrescrever o equals o hasCode também deve ser sobrescrito. 
      Fonte.(Luckow e  Melo, 2015). 
      
*/  
package br.com.auladehibernate.conta;

import br.com.auladehibernate.cliente.Cliente;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 *
 * @author joseluiz
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name="conta")
@SequenceGenerator(name="seqConta",sequenceName="seq_conta")
public class Conta implements  Serializable{
    private static final long serialVersionUID = 2566460635793741414L;
    
    @Id
    @GeneratedValue(generator="seqConta",strategy= GenerationType.AUTO)
    @Column(name="id_conta")
    private Integer idConta;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @OnDelete(action= OnDeleteAction.NO_ACTION)
    @JoinColumn(name="id_cliente",nullable=false)
    private Cliente cliente;
    
    @Column(name="tipo")
    private Integer tipo;

    public Integer getIdConta() {
        return idConta;
    }

    public void setIdConta(Integer idConta) {
        this.idConta = idConta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.idConta);
        hash = 29 * hash + Objects.hashCode(this.cliente);
        hash = 29 * hash + Objects.hashCode(this.tipo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Conta other = (Conta) obj;
        if (!Objects.equals(this.idConta, other.idConta)) {
            return false;
        }
        if (!Objects.equals(this.cliente, other.cliente)) {
            return false;
        }
        if (!Objects.equals(this.tipo, other.tipo)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
