/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.auladehibernate.conta;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author jose
 */
@Entity
@PrimaryKeyJoinColumn(name="idConta")
@Table(name="conta_limite")
public class ContaLimite extends Conta implements Serializable{
    private static final long serialVersionUID = -2054881840256988429L;
    
    @Column(name="saldo")
    private Double limite;

    public Double getLimite() {
        return limite;
    }

    public void setLimite(Double limite) {
        this.limite = limite;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.limite);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ContaLimite other = (ContaLimite) obj;
        if (!Objects.equals(this.limite, other.limite)) {
            return false;
        }
        return true;
    }
    
    
}
