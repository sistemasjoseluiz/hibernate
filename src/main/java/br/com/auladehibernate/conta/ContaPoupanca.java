/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.auladehibernate.conta;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author jose
 */
@Entity
@PrimaryKeyJoinColumn(name="idConta")
@Table(name="conta_poupanca")
public class ContaPoupanca extends Conta implements Serializable{
  
    private static final long serialVersionUID = -2328480613899672302L;
    
    @Column(name="saldo")
    private Double saldo;

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.saldo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ContaPoupanca other = (ContaPoupanca) obj;
        if (!Objects.equals(this.saldo, other.saldo)) {
            return false;
        }
        return true;
    }
    
    
    
}
